<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<title><?php
		if( isset( $_GET['date'] ) && preg_match('/^[0-9]{4}-[0-9]{2}$/', $_GET['date'] ) ){
			$date = $_GET['date'];
			echo date( 'M Y', strtotime( $date ) ) . ' - Intelligent Security';
		} else {
			echo 'Intelligent Security Newsletter Archive';
		}
		?></title>
		<style type="text/css">
		#emailContainer{max-width: 640px; width: 100%;}
		#emailContainer tbody tr + tr td img[style],
		#emailContainer tbody tr + tr + tr + tr td img[style]{

		}
		@media screen and (max-width: 720px){
			#bodyTable > tbody > tr > td[style]{
				padding: 0;
				background-color: #fff !important;
			}
			#emailContainer tbody tr + tr td img[style],
			#emailContainer tbody tr + tr + tr + tr td img[style]{
				width: 100%;
				height: auto;
				margin: 0 0 1rem 0;
			}
			#header-headline td[style]{
				font-size: 14px !important;
				padding: 4px 8px !important;
			}
			.faux_p[style]{
				font-size: 1.25rem !important;
				line-height: 1.5rem !important;
			}
		}
		</style>
	</head>
	<body>
	<?php
	if( isset( $_GET['date'] ) && preg_match('/^[0-9]{4}-[0-9]{2}$/', $_GET['date'] ) && isset( $_SERVER['HTTP_REFERER'] ) ){
		$backlink = $_SERVER['HTTP_REFERER'];
	} else {
		$backlink = 'http://'. $_SERVER['HTTP_HOST'].'/newsletter/';
	}

	echo '<p style="text-align: center; padding-top: 10px; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;"><a href="' . $backlink . '" style="color: #999">Back to Newsletter Archive</a></p>';
	?>