<?php
require_once( 'fns.utilities.php' );

if( ! isset( $_GET['date'] ) || ! preg_match('/^[0-9]{4}-[0-9]{2}$/', $_GET['date'] ) ){
	include( 'header.php' );
	echo '<div style="max-width: 700px; width: 90%; margin: 20px auto"><h2>Intelligent Security Newsletter Archive</h2>';
	//echo '<a href="http://' . $_SERVER['HTTP_HOST'] . '/newsletter/" class="button black small bordered-bot " target="_self" style="display: block; float: right; margin: 0 0 10px 20px;">Subscribe</a>';
	$newsletters = glob( 'html/intelligent-security*.html');
	$newsletters = array_reverse( $newsletters );
	if( is_array( $newsletters ) ){
		echo '<ul>';
		foreach( $newsletters as $newsletter ){
			preg_match( '/[0-9]{4}-[0-9]{2}/', $newsletter, $matches );
			$date = $matches[0];
			$url = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF'];
			echo '<li><a href="' . $url . '?date=' . $date . '">Intelligent Security - ' . date( 'M Y', strtotime( $date ) ) . '</a></li>';
		}
		echo '</ul>';
	} else {
		echo '<p>No newsletters found!</p>';
	}
	echo '</div>';
	include( 'footer.php' );
} else {
	/**
	 * HEADER/FOOTER REQUIRED?
	 *
	 * If we ever adjust the email HTML build process
	 * such that we no longer need the header/footer
	 * when we display the email online, the following
	 * code allows for setting a cutoff date for
	 * including/not including the header/footer.
	 */
	if( 'template' == $_GET['date'] ){
		$difference = -1;
		$filename = 'template.html';
	} else {
		$date_requested = new DateTime( $_GET['date'] );
		$date_compared = new DateTime( '2014-12-31' ); // cutoff date
		$interval = $date_requested->diff( $date_compared );
		$difference = $interval->format( '%R%a' );
		// ( $difference > 0 ) - requires header/footer
		// ( $difference < 0 ) - does NOT require header/footer

		$filename = 'intelligent-security_' . $_GET['date'] . '.html';
	}

	$filename = 'html/' . $filename;

	if( file_exists( $filename ) && ( $difference > 0 ) || ! file_exists( $filename ) ){
		include( 'header.php' );
	} else if( file_exists( $filename ) && ( $difference < 0 ) ){
		include( 'header.2015-01.php' );
	}

	// grab newsletter HTML
	if( file_exists( $filename ) ){
		$newsletter = file_get_contents( $filename );
		if( $difference > 0 )
			$newsletter = wpautop( $newsletter, true );
		$newsletter = preg_replace( '/[\x00-\x1F\x80-\xFF]/', '', $newsletter ); // Remove non printable unicode characters
		echo $newsletter;
	} else {
		echo '<p>File not found (<em>' . $filename . '</em>)!</p>';
	}

	include( 'footer.php' );
}
?>