module.exports = function(grunt) {
  require('load-grunt-tasks')(grunt); // npm install --save-dev load-grunt-tasks

  grunt.initConfig({
    watch: {
      less:{
        files: ['html/*.html','header.php','header.2015-01.php','newsletter.php'], // which files to watch
        options: {
          livereload: true
        }
      }
    }
  });

  grunt.registerTask('default', ['watch']);
};