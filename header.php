<html>
	<head>
		<link rel="stylesheet" type="text/css" href="//<?php echo $_SERVER['HTTP_HOST'] ?>/wp-content/themes/florida-wp-swds/lib/css/main.css">
		<style type="text/css">
		.footer td{text-align: center; text-size: 11px; color: #999;}
		h2{font-size: 18px; line-height: 1.5em;}
		.logo{width: 700px; height: 190px;}
		table, table td, table th{border: none;}
		table{margin: 0 auto; background-color: #fff;}
		ul{margin-top: 10px;}
		#emailContainer{max-width: 640px; width: 100%;}
		</style>
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<title><?php
		if( isset( $_GET['date'] ) && preg_match('/^[0-9]{4}-[0-9]{2}$/', $_GET['date'] ) ){
			$date = $_GET['date'];
			echo date( 'M Y', strtotime( $date ) ) . ' - Intelligent Security';
		} else {
			echo 'Intelligent Security Newsletter Archive';
		}
		?></title>
	</head>
	<body>
	<?php
	if( isset( $_GET['date'] ) && preg_match('/^[0-9]{4}-[0-9]{2}$/', $_GET['date'] ) && isset( $_SERVER['PHP_SELF'] ) ){
		$backlink = $_SERVER['PHP_SELF'];
	} else {
		$backlink = 'http://'. $_SERVER['HTTP_HOST'].'/newsletter/';
	}
	echo '<p style="text-align: center; padding-top: 10px;"><a href="' . $backlink . '">Back to Newsletter Archive</a></p>';
	?>